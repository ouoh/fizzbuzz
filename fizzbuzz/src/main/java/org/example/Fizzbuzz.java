package org.example;

public class Fizzbuzz {

    public String fizz(int nombre){
        if(nombre<10 && nombre>0){
            if(nombre==3){
                return "fizz";
            }
        }
        return "";
    }
    public String buzz(int nombre){
        if(nombre<10 && nombre>0){
            if(nombre==5){
                return "buzz";
            }
        }
        return "";
    }

    public String sprint1(int nombre){
        String bfizz = fizz(nombre);
        String bbuzz = buzz(nombre);
        if(!"".equals(bfizz)){
            return bfizz;
        }
        if(!"".equals(bbuzz)){
            return bbuzz;
        }
        return "";
    }

    public String fizz2(int nombre){
        if(nombre<=10 && nombre>0){
            if(nombre%3==0){
                return "fizz";
            }
        }
        return "";
    }

    public String buzz2(int nombre){
        if(nombre<=10 && nombre>0){
            if(nombre%5==0){
                return "buzz";
            }
        }
        return "";
    }

    public String sprint2(int nombre){
        String bfizz = fizz2(nombre);
        String bbuzz = buzz2(nombre);
        if(!"".equals(bfizz)){
            return bfizz;
        }
        if(!"".equals(bbuzz)){
            return bbuzz;
        }
        return "";
    }

    public String sprint3(int nombre){
        if(nombre<101 && nombre>0){
            if(nombre%3==0 && nombre%5==0){
                return "fizzbuzz";
            }
        }
        return "";
    }

    public String sprint4(int nombre){
        String result;
        int fizz_or_buzz;
        result = sprint2(nombre) + sprint3(nombre);
        for(int i=0;i<String.valueOf(nombre).length();i++){
            fizz_or_buzz = Character.getNumericValue(String.valueOf(nombre).charAt(i));
            if(fizz_or_buzz==3){
                result+="fizz";
            }
            if(fizz_or_buzz==5){
                result+="buzz";
            }
        }
        return result;
    }

    public static void main(String[] args) {

    }

}
