package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    @Test
    public void testFizz(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.fizz(3);
        Assertions.assertEquals("fizz",result);
    }
    @Test
    public void testBuzz(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.buzz(5);
        Assertions.assertEquals("buzz",result);
    }
    @Test
    public void testSprint1fizz(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint1(3);
        Assertions.assertEquals("fizz",result);
    }
    @Test
    public void testSprint1buzz(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint1(5);
        Assertions.assertEquals("buzz",result);
    }
    @Test
    public void testFizz2(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.fizz2(9);
        Assertions.assertEquals("fizz",result);
    }
    @Test
    public void testBuzz2(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.buzz2(10);
        Assertions.assertEquals("buzz",result);
    }

    @Test
    public void testSprint2fizz(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint2(9);
        Assertions.assertEquals("fizz",result);
    }
    @Test
    public void testSprint2buzz(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint2(10);
        Assertions.assertEquals("buzz",result);
    }
    @Test
    public void testSprint3(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint3(15);
        Assertions.assertEquals("fizzbuzz",result);
    }
    @Test
    public void testSprint4_1(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint4(3);
        Assertions.assertEquals("fizzfizz",result);
    }
    @Test
    public void testSprint4_2(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint4(35);
        Assertions.assertEquals("fizzbuzz",result);
    }
    @Test
    public void testSprint4_3(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint4(53);
        Assertions.assertEquals("buzzfizz",result);
    }
    @Test
    public void testSprint4_4(){
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        String result = fizzbuzz.sprint4(30);
        Assertions.assertEquals("fizzbuzzfizz",result);
    }
}
